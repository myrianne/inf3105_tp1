/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault BEAM26588114             */

//------------------------------------------------------- HEURE.CPP

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "heure.h"

//------------------------------------------------------- CONSTRUCTEUR ()
Heure::Heure(int h, int m, int s)
  : heures(h), minutes(m), secondes(s=0) {
}

//------------------------------------------------------- CONSTRUCTEUR PAR COPIE()
Heure& Heure::operator = (const Heure& autre) {

    if(this==&autre) return *this;
    heures = autre.heures;
    minutes = autre.minutes;
    secondes = autre.secondes;
    
    return *this;
}

//------------------------------------------------------ MÊME HEURE ? ()
bool Heure::operator == (const Heure& h) const {

	if(heures == h.heures) {
		if(minutes == h.minutes) return true;
	}
	
    return false;
}

//------------------------------------------------------ PLUS TÔT ? ()
bool Heure::operator < (const Heure& h) const{

	int hres = heures, hres_2 = h.heures;
	if(hres < hres_2) return true;
	if(hres == hres_2) {
		if(minutes < h.minutes) return true;
		if(minutes == h.minutes) {
			if(secondes < h.secondes) return true;
		}
	}
    return false;
}

//------------------------------------------------------ CALCULER L'HEURE ()
Heure Heure::operator + (const int metres) {

	Heure lHeure;
	int s_totales = metres/10;
	int h = s_totales/3600;
	int m = (s_totales -(3600 * h))/60;
	int s = (s_totales -(3600 * h))%60;
	int secs = secondes + s;
	int mins = minutes + m;
	int hres = heures + h;
	
	// Ajuster secondes et minutes si plus de 59 secondes
	if(secs > 59) {
		lHeure.secondes = secs - 60;
		mins++;
		
	} else {
		lHeure.secondes =  secs;
	}
	// Ajuster minutes et heures si plus de 59 minutes
	if(mins > 59) {
		lHeure.minutes = mins - 60;
		hres++;
	} else {
		lHeure.minutes =  mins;
	}
	lHeure.heures =  hres;

    return lHeure;
}

//------------------------------------------------------ LIRE L'HEURE ?()
std::istream& operator >> (std::istream& is, Heure& h){
    
    char c;

    is >> h.heures;
    is >> c >> h.minutes;
    
    return is;
}

//------------------------------------------------------ ÉCRIRE L'HEURE ()
std::ostream& operator << (std::ostream& os, const Heure& h){
	
	char chaine[40];
	
    sprintf(chaine, "%02dh%02dm%02ds", h.heures, h.minutes, h.secondes);
    os << chaine;
    
    return os;
}