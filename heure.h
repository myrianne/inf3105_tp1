/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault BEAM26588114             */

//------------------------------------------------------- HEURE.H

#if !defined(__HEURE_H__)
#define __HEURE_H__
#include <iostream>

//------------------------------------------------------- CLASSE HEURE
class Heure{

  public:
    Heure(){ secondes= 0; }
    Heure(int h, int m, int s);
	Heure& operator = (const Heure& autre);
    bool operator == (const Heure& h) const;
    bool operator < (const Heure& h) const;
    Heure operator + (const int metres);
    
  private:	
	int heures;
	int minutes;
	int secondes;
	
  friend std::istream& operator >> (std::istream&, Heure&);
  friend std::ostream& operator << (std::ostream&, const Heure&);
};

#endif