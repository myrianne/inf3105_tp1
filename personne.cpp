/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault BEAM26588114             */

//------------------------------------------------------- PERSONNE.CPP

#include <cassert>
#include <string>
#include "personne.h"
#include "heure.h"

//------------------------------------------------------- CONSTRUCTEUR ()
Personne::Personne(std::string n, int nb, char p, PointST o, PointST d, Heure h1, Heure h2, Heure h3, Heure h4)
  : nom(n), nb(nb), pret(p), o(o), d(d), hr1(h1), hr2(h2), hr3(h3), hr4(h4)
{
}

//------------------------------------------------------- LIRE UNE PERSONNE ()
std::istream& operator >> (std::istream& is, Personne& p){
    std::string s, h1, h2, h3, h4;
	char c;
	
    is >> p.nom >> p.nb >> p.pret;
    assert(p.nb > 1);
    assert(p.pret == 'O' || p.pret == 'N');
    is >> p.o >> s >> p.d; 
    assert(s == "-->");
    is >> p.hr1 >> p.hr2 >> p.hr3 >> p.hr4 >> c;
    assert(c== ';');
    
    return is;
}

//------------------------------------------------------- ÉCRIRE UNE PERSONNE ()
std::ostream& operator << (std::ostream& os, const Personne& p){

    os  <<  p.nom << '\t' << p.nb << '\t' << p.pret;
    os  << '\t' << p.o << '\t' << "-->" << '\t' << p.d;
    os  << '\t' << p.hr1 << '\t' << p.hr2 << '\t' << p.hr3 << '\t' << p.hr4 << std::endl;
    
    return os;
}