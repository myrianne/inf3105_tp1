/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault BEAM26588114             */

//------------------------------------------------------- PERSONNE.H

#if !defined(__PERSONNE_H__)
#define __PERSONNE_H__
#include <iostream>
#include "heure.h"
#include "pointst.h"

//------------------------------------------------------- CLASSE PERSONNE
class Personne{

  public:
    Personne(){}
    Personne(std::string n, int nb, char p, PointST o, PointST d, Heure h1, Heure h2, Heure h3, Heure h4);
    std::string 	nom;
	int 	nb;
	char 	pret;
	PointST o, d;
	Heure 	hr1, hr2, hr3, hr4;
	Heure creerHeure(std::string h);
	
  private:

  friend std::istream& operator >> (std::istream&, Personne&);
  friend std::ostream& operator << (std::ostream&, const Personne&);
};

#endif