/*  INF3105 | Structures de données et algorithmes (Automne 2018)
    UQAM | Département d'informatique
    http://ericbeaudry.uqam.ca/INF3105/ */
    
// NE PAS IMPRIMER !!

//------------------------------------------------------- POINTST.CPP

#include <assert.h>
#include <cmath>
#include "pointst.h"
#define PI 3.14159265359
#define RAYON_TERRE 6371 * 1000

//------------------------------------------------------- CONSTRUCTEUR avec point()
PointST::PointST(const PointST& point)
  : latitude(point.latitude), longitude(point.longitude)
{
}

//------------------------------------------------------- CONSTRUCTEUR avec lat et long ()
PointST::PointST(double latitude_, double longitude_) 
  : latitude(latitude_), longitude(longitude_)
{
}

//------------------------------------------------------- DISTANCE ()
double distance(const PointST& a, const PointST& b) {
  double s1 = sin((b.latitude-a.latitude)/2);
  double s2 = sin((b.longitude-a.longitude)/2);
  return 2*RAYON_TERRE * asin(sqrt(s1*s1 + cos(a.latitude)*cos(b.latitude)*s2*s2));
}

//------------------------------------------------------- PLUS PETIT ? ()
bool PointST::operator < (const PointST& a) const {
  return (latitude < a.latitude && longitude > a.longitude);
}

//------------------------------------------------------- LIRE POINT ()
std::ostream& operator << (std::ostream& os, const PointST& point) {
  os << "(" 
     << (point.latitude * 180.0 / PI)
     << "," 
     << (point.longitude * 180.0 / PI)
     << ")";
  return os;
}

//------------------------------------------------------- ÉCRIRE POINT ()
std::istream& operator >> (std::istream& is, PointST& point) {
  char po, vir, pf;
  is >> po;
  if(is){
    is >> point.latitude >> vir >> point.longitude >> pf;
    assert(po=='(');
    assert(vir==',');
    assert(pf==')');
    point.latitude  *= PI / 180.0;
    point.longitude *= PI / 180.0;
  }
  return is;
}