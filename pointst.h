/*  INF3105 | Structures de données et algorithmes (Automne 2018)
    UQAM | Département d'informatique
    http://ericbeaudry.uqam.ca/INF3105/ */
    
// NE PAS IMPRIMER !!

//------------------------------------------------------- POINTST.H

#if !defined(_POINTST__H_)
#define _POINTST__H_
#include <iostream>

//------------------------------------------------------- CLASSE POINTST
class PointST {

  public:
    PointST(){}
    PointST(double latitude_, double longitude_);
    PointST(const PointST&);
	bool operator < (const PointST& a) const;  

  private:
    double latitude;  
    double longitude;

  friend std::ostream& operator << (std::ostream&, const PointST&);
  friend std::istream& operator >> (std::istream&, PointST&);
  friend double distance(const PointST&, const PointST&);
};

#endif