/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault BEAM26588114             */

//------------------------------------------------------- TABLEAU.H

#if !defined(_TABLEAU___H_)
#define _TABLEAU___H_
#include <assert.h>
//using namespace std;

//------------------------------------------------------- CLASSE TABLEAU
template <class T>
class Tableau{

  public:
    Tableau(int capacite_initiale=4);
    Tableau(const Tableau& autre);
    ~Tableau();
    void           ajouter(const T& item);
    void           vider();
    int            taille() const;
    void 		   redimensionner(int nouvCapacite);
    void           inserer(const T& element, int index=0);
    void           enlever(int index=0);
    int            chercher(T& element);
    const T&       operator[] (int index) const;
    T&             operator[] (int index);
    bool           operator == (const Tableau<T>& autre) const;
    Tableau<T>&    operator = (const Tableau<T>& autre);

  private:
    T*             elements;
    int            nbElements;
    int            capacite;
};

//------------------------------------------------------- CONSTRUCTEUR ()
template <class T>
Tableau<T>::Tableau(int capacite_) {
    capacite = capacite_;
    nbElements = 0;
    elements = new T[capacite];
}

//------------------------------------------------------- CONSTRUCTEUR PAR COPIE ()
template <class T>
Tableau<T>::Tableau(const Tableau<T>& autre) {
    capacite = autre.capacite;
    nbElements = autre.nbElements;
    elements = new T[capacite];
    for(int i=0; i<nbElements;i++) elements[i] = autre.elements[i];
}

//------------------------------------------------------- DESTRUCTEUR ()
template <class T>
Tableau<T>::~Tableau() {
  delete[] elements;
  elements = NULL;
}

//------------------------------------------------------- TAILLE ()
template <class T>
int Tableau<T>::taille() const {
    return nbElements;
}

//------------------------------------------------------- AJOUTER ()
template <class T>
void Tableau<T>::ajouter(const T& item) {
  if(nbElements >= capacite) redimensionner(capacite * 2);
  elements[nbElements++] = item; 
}

//------------------------------------------------------- REDIMENSIONNER ()
template <class T>
void Tableau<T>::redimensionner(int nouvCapacite) {
  capacite = nouvCapacite;
  T* temp = new T[capacite];
  for(int i=0; i<nbElements;i++)temp[i] = elements[i];
  delete[] elements;
  elements = temp;
}

//------------------------------------------------------- INSERER ()
template <class T>
void Tableau<T>::inserer(const T& element, int index) {
    T* temp = new T[capacite+1];
    int fin = nbElements +1;
    int j=0, i=0;
    do {
      if(i == index) temp[i++] = element;
      temp[i++] = elements[j++];
    } while (i<=fin);
    nbElements++;
    delete[] elements;
    elements = temp;
}

//------------------------------------------------------- ENLEVER ()
template <class T>
void Tableau<T>::enlever(int index) {
	int j=0;
    T* temp = new T[nbElements -1];
    for(int i=0; i<nbElements-1; i++) {
      if(i == index) {
      	temp[i] = elements[++j];
      }
      temp[i] = elements[j++];
    }
    nbElements--;
    delete[] elements;
    elements = temp;
}

//------------------------------------------------------- CHERCHER ()
template <class T>
int Tableau<T>::chercher(T& element) {
    for(int i=0; i<nbElements; i++) {
      if(elements[i] == element) return i;
    }
    return -1;
}

//------------------------------------------------------- VIDER ()
template <class T>
void Tableau<T>::vider() {
  nbElements = 0;
}

//------------------------------------------------------- [] CONST
template <class T>
const T& Tableau<T>::operator[] (int index) const {
    assert(index<nbElements);
    return elements[index];
}

//------------------------------------------------------- []
template <class T>
T& Tableau<T>::operator[] (int index) {
    assert(index<nbElements);
    return elements[index];
}

//------------------------------------------------------- =
template <class T>
Tableau<T>& Tableau<T>::operator = (const Tableau<T>& autre) {
    if(this==&autre) return *this;
    nbElements = autre.nbElements;
    if(capacite < autre.nbElements){
      delete[] elements;
      capacite = autre.nbElements;
      elements = new T[capacite];
    }
    for(int i=0; i<nbElements;i++){
      elements[i] = autre.elements[i];
    }
    return *this;
}

//------------------------------------------------------- ==
template <class T>
bool Tableau<T>::operator == (const Tableau<T>& autre) const
{
    if(nbElements != autre.nbElements) return false;
    for(int i=0; i<nbElements; i++) {
      if(elements[i] != autre.elements[i]) return false;
    }
    return true;
}

#endif