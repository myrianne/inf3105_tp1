/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  Myrianne Beaudoin-Thériault #BEAM26588114            */

//---------------------------------------------------------- TP1.CPP

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include "personne.h"
#include "tableau.h"
#include "pointst.h"

// --------------------------------------------------------- FXS()
int tp1(std::istream& entree);
int allerChercher(Tableau<Personne> personnes);
int afficherRecommandation(Tableau<Personne> in, Tableau<Personne> out);
double calculerDistance(Tableau<Personne> in, Tableau<Personne> out);
double getDistanceUnePersonne(Personne p1);
double getDistanceAvecPret(Personne p1, Personne p2);
double getDistanceSansPret(Personne p1, Personne p2);
bool verifierHeures(Tableau<Personne> in, Tableau<Personne> out);

// --------------------------------------------------------- MAIN()
int main(int argc, const char** argv) {
    if(argc>1){
        std::ifstream fichier_entree(argv[1]);
        if(fichier_entree.fail()){
            std::cerr << "Erreur de lecture du fichier '" << argv[1] << "'" << std::endl;
            return 1;
        }
        return tp1(fichier_entree);
    }else{
        return tp1(std::cin);
    }
}

// --------------------------------------------------------- TP1()
int tp1(std::istream& entree_requetes) {
	Tableau<Personne> personnes;
	
    // Lecture des données entrées
    while(entree_requetes && !entree_requetes.eof()){
		Personne p;
		
		// Ajout des personnes à un tableau
        entree_requetes >> p >> std::ws;
        personnes.ajouter(p);
    }
    allerChercher(personnes);
    return 0;
}

// --------------------------------------------------------- ALLER_CHERCHER()
int allerChercher(Tableau<Personne> tabPersonnes){
	// Recommandation pour chaque personne
	Tableau<Personne> personnes = tabPersonnes;
    for(int i=0; i<personnes.taille(); i++) {
    	Tableau<Personne>in, out_N, out, theOut, theIn, bestIn, bestOut, clients= personnes, trajet;
    	Personne proprio = personnes[i], p1= personnes[i];
    	double d_min=distance(proprio.o, proprio.d)*2, d_N=0, d_O=0;
    	
    	// Ajouter proprio au tableau d'embarquements
    	in.ajouter(proprio);
    	clients.enlever(i);
    	std::cout << proprio.nom << ':' << '\t'; 
    	 
    	// Ajouter autre personne au tableau d'embarquements sans et avec pret
		for(int j=0; j<clients.taille(); j++) {
			in.ajouter(clients[j]);
			theIn = in;
			out.vider();
			out_N.vider();
			
			// Ajouter personne au tableau de débarquements avec pret et sans pret
			for(int k=0; k<in.taille() && proprio.pret!='N'; k++) out.ajouter(in[k]);
			for(int k=in.taille()-1; k>=0; k--) out_N.ajouter(in[k]);
			d_O = 0;
			d_N = 0;
			
			// Calculer la distance seulement si heuresVérifier == ok
			if(proprio.pret == 'O' && verifierHeures(in, out))	d_O = calculerDistance(in, out);
			if (verifierHeures(in, out_N))  d_N = calculerDistance(in, out_N);
			
			// Calculer la meilleure distance
			if(d_O != 0 && d_N != 0) { 
				if(d_N < d_O) theOut = out_N;	
				else if(d_O < d_N) theOut = out;		
			} 
			else if(d_O!=0 && d_N==0) theOut = out;	
			else if(d_O==0 && d_N!=0 ) theOut = out_N;	
			if((d_O != 0 || d_N != 0) && (calculerDistance(theIn, theOut) < d_min )) {
				bestIn = theIn;
				bestOut = theOut;
				d_min = calculerDistance(theIn, theOut);
			}
			// Mettre à jour le tableau d'embarquements 
			in.vider();
			in.ajouter(proprio);
		}
		// Créer tableau proprio, seulement si il n'y a aucune recommendation
		if (bestOut.taille() == 0) bestIn.vider();
		if (bestIn.taille() == 0) {
			bestIn.ajouter(proprio);
			bestOut.ajouter(proprio);
			d_min = getDistanceUnePersonne(proprio);
		}
		afficherRecommandation(bestIn, bestOut);
		std::cout << (round(d_min)) << 'm' << std::endl;	
    }
    return 0;
}


// --------------------------------------------------------- AFFICHER_RECOMMANDATION()		
int afficherRecommandation(Tableau<Personne> in, Tableau<Personne> out) {
		// Afficher le nom des personnes embarquées à l'allée
		for(int i=0; i< in.taille(); i++) std::cout << "+" << in[i].nom;
		
		// Afficher le nom des personnes débarquées à l'allée
		for(int i=0; i< out.taille(); i++) std::cout << "-" << out[i].nom;
		std::cout << "\t";
		
		// Afficher le nom des personnes embarquées au retour
		for(int i=out.taille()-1; i>= 0; i--) std::cout << "+" << out[i].nom;
		
		// Afficher le nom des personnes débarquées au retour
		for(int i=in.taille()-1; i>= 0; i--) std::cout << "-" << in[i].nom;
		std::cout << "\t";
		return 0;		
}

// --------------------------------------------------------- CALCULER DISTANCE
double calculerDistance(Tableau<Personne> in, Tableau<Personne> out) {	
	bool pret = !(in[0].nom == out[out.taille()-1].nom);
	Tableau<Personne> trajet=in;
	Personne p2, p1 = in[0];
	double laDistance=0;
	trajet.enlever(0);
	if(in.taille() == 1) {
		laDistance = 2*(distance(p1.o, p1.d));
	} else {
		
		// Calculer distance pour chaque personne embarquée
		for(int i=0; i<trajet.taille(); i++) {
			p2 = trajet[i];
			
			// Calculer distance selon prêt == 'O' ou 'N'
			if(!pret) laDistance += getDistanceSansPret(p1, p2);
			else  laDistance += getDistanceAvecPret(p1, p2);
			
		}
	}
		
	return laDistance;
}
// --------------------------------------------------------- GET_DISTANCE_AVEC_PRET()
double getDistanceUnePersonne(Personne p1) {
	return 2*(distance(p1.o, p1.d));
}
// --------------------------------------------------------- GET_DISTANCE_AVEC_PRET()
double getDistanceAvecPret(Personne p1, Personne p2) {
	return 2*(distance(p1.o, p2.o) + distance(p2.o, p1.d) + distance(p1.d, p2.d) - distance(p2.o, p2.d));
}

// --------------------------------------------------------- GET_DISTANCE_SANS_PRET()
double getDistanceSansPret(Personne p1, Personne p2) {
	return 2*(distance(p1.o, p2.o) + distance(p2.o, p2.d) + distance(p2.d, p1.d) - distance(p2.o, p2.d));
}

// --------------------------------------------------------- VERIFIER_HEURES()
bool verifierHeures(Tableau<Personne> in, Tableau<Personne> out) {
	Tableau<Personne> in_r, out_r;
	Personne p1=in[0], p2=in[1];
	Heure lHeure = p1.hr1;

	// Créer tableau retour
	for(int i=out.taille()-1; i>=0; i--) in_r.ajouter(out[i]);
	for(int i=in.taille()-1; i>=0; i--) out_r.ajouter(in[i]);

	if (in.taille() == 1) return true;

	// ALLÉE 
	// embarquements
	p1 = in[0];
	for(int i=1; i<in.taille(); i++) {
		p2=in[i];
		lHeure = lHeure + distance(p1.o, p2.o);
		if( p1.hr2 <  p2.hr1) return false;
		if(lHeure < p2.hr1) lHeure = p2.hr1;	
		p1 = p2;
	}
		
	// transit
	p1 = in[in.taille()-1];
	p2 = out[0];
	lHeure = lHeure + distance(p1.o, p2.d);
	if(p2.hr2 < lHeure) return false;

	// débarquements
	p1 = out[0];
	for(int i=1; i<out.taille(); i++) {
		p2=out[i];
		lHeure = lHeure + distance(p1.d, p2.d);
		if (p2.hr2 < lHeure) return false;
		if (p2.hr2 == lHeure) return false;
		p1 = p2;
	}
		
	// RETOUR 
	// embarquements	
	p1 = in_r[0];
	lHeure = p1.hr3;
	for(int i=1; i<in_r.taille(); i++) {
		p2=in_r[i];
		lHeure = lHeure + distance(p1.d, p2.d);
		if(  p2.hr4 < lHeure ) return false;
		if(lHeure < p2.hr3) lHeure = p2.hr3;
		p1 = p2;
	}
		
	// transit
	p1 = in_r[in_r.taille()-1];
	p2 = out_r[0];
	lHeure = lHeure + distance(p1.d, p2.o);
	if(p2.hr4 < lHeure) return false;
	if(p2.hr4 == lHeure) return false;
		
	// débarquements
	p1 = out_r[0];
	for(int i=1; i<out_r.taille(); i++) {
		p2=out_r[i];
		lHeure = lHeure + distance(p1.o, p2.o);
		if (p2.hr4 < lHeure) return false;
		if (p2.hr4 == lHeure) return false;
		p1 = p2;
	}	
		
	return true;
}